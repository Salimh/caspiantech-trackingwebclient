import {Component, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Tracking Web Client';
  cssUrl: string;
  constructor(public sanitizer: DomSanitizer,
              public translate: TranslateService) {
    this.translate.addLangs(['en', 'fa']);
    this.translate.setDefaultLang('fa');
  }
  ngOnInit(): void {
    this.cssUrl = '/assets/css/style-rtl.css';

    const browserLang = this.translate.getBrowserLang();
    this.translate.use('fa');
  }
}
