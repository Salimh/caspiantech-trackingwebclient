import {HomeComponent} from './components/home/home.component';
import {Routes} from '@angular/router';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';

export class AppRoutes {
  static routes: Routes = [
    { path: 'home', component: HomeComponent},
    { path: '', component: HomeComponent, pathMatch: 'full'},
    { path: '**', component: PageNotFoundComponent}
  ];
}
